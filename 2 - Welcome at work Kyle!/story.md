The light in the room was quite annoying, it was broken. It was difficult for the agent Ophelia to pay attention to the declaration of the victim who was quite nervous.

“I... I don’t understand” the victim said. He was a normal user of the Hedyl facilities for keeping electronic devices before shipping and check their performance. “I just executed a normal script a use everyday, I wrote it you know? It’s called goodmorning. But this... this time it just blew everything up! I didn’t do anything I swear.”

“Okay don’t worry. So someone managed to enter in your account right? Is it the same as root?” Ophelia asked while opening her laptop and turning it on. 

“No... root is different. I have a different account, KG_9941” He was looking at the laptop. “Am I in trouble?”

“No, let’s focus.” Ophelia was typing “The forensic agent gave me a quick report before going further let’s see... Here it says that the attacker got access to root. He went as fast as possible and forgot to cover some clues, such as the history log. We can see here he got access to your account and then typed alias goodmorning = ‘./boom.sh’ in the ~/.bashrc.” she looked at him over the screen. 

“Oh man...”

“Sir, did you notice anything weird that day or the night before? Apparently we don’t know what ‘boom.sh’ script had, as the attacker removed it. Maybe we can start looking for that program, if there’s still any trace of it.”

The man thought for a moment in silence. 

“Not that but... You know the program was acting exactly they way it should, at first. I mean when it starts it says ‘welcome again at work, master Kyle’ and then I start doing my stuff...” He looked a bit proud at first but then he went all blushed. “It’s just a joke you know, the welcome thing”

“No worries, my sudo is alias as please. I enjoy that jokes too” She said trying to make it easier for the guy. “Let’s focus on that... can I see your normal script?”

“Sure thing. I posted both the source code and the executable on gitlab.” He wrote something in a notebook and passed it to Ophelia. “Here it is.”

In the notebook was written a [link](https://gitlab.com/terceranexus6/mastodon_challenges/tree/master/2%20-%20Welcome%20at%20work%20Kyle!/Kyle_script).

“Thank you.” she downloaded the content and took a look, it was written in C. Ophelia then realized. “Ah! The name is not written by default!” she said.

“No... I showed it to my workmates and some of them wanted to use it too... So I just changed it in order they all can use it.”

“Wait wait so... all your workmates knew you used this in the morning?” She started to make sense of the situation. It was clear now.

“Y...yes? It’s such a harmless script you know, nothing relevant.”

“I know... Kyle, probably the attacker is a workmate. Or someone close to your workmates, who new this routine. Can you think of someone?”

“I... I don’t know.” Kyle seemed sad, probably thinking any of his workmates would do such thing.

“I think I know how this happened. Let me try a thing...” she compiled it and typed `./goodmorning AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA` and it returned segmentation fault. “And the report here says something else... Apparently also the actual goodmorning script was executed. It’s not on the history but it’s on the ps log you all guys have.”

“Wha...what? really? That’s weird, why?”

“The attacker took advantage of it... Only he didn’t use the name as the parameter...” She fast typed a lot, Kyle seemed uncomfortable. After a while she finally said “Ha!”

“What?”

“I know how was it, Kyle.”


Agent Ophelia used a script she made which looked like this:

```

#include <stdio.h>

unsigned long get_sp(void){
  __asm__("movl %esp, %eax");
}

void main(){
  printf("0x%x\n", get_sp() );
}

```

This returned a lot of outputs, but the one that made Ophelia sharply smile was 0x08048444. What was the attack?