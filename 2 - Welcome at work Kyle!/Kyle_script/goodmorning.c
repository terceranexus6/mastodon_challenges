#include <string.h>
#include <stdio.h>

void func(char *arg) {
  char name[32];
  strcpy(name, arg);
  printf("\nWelcome at work, master %s\n\n", name);
}

int main(int argc, char *argv[]){
  if ( argc != 2 ){
    printf("Use: %s NAME\n",argv[0]);
    //exit(0);
  }
  func(argv[1]);
  printf("Good luck today.\n\n");
  return 0;
}